
import javax.swing.JOptionPane;
public class Cadenes_6 {

	public static void main(String[] args) {

		// Declaracions de variables
		String buscarCadena="",cadena="";
		int posicio=0, i, j;
		boolean trobada=false;
		
		try {

			// Demano una cadena d'entrada
			cadena = JOptionPane.showInputDialog(null,"Introdueix una cadena: ","Cadena d'entrada:",JOptionPane.QUESTION_MESSAGE);

			// buscarCadena a cadena
			buscarCadena = JOptionPane.showInputDialog(null,"Cadena a buscar: ","Cadena a buscar:",JOptionPane.QUESTION_MESSAGE);

			// ERROR: cadena a buscar, més llarga que la cadena.
			if(buscarCadena.length() >= cadena.length()) throw new Exception("Error: cadena a buscar, més llarga que la cadena.");

			for (i = 0; i < cadena.length(); i++) {
				
				// Si no hi ha coincidencia, salta d'iteració
				if (!(cadena.charAt(i) == buscarCadena.charAt(posicio))) continue;

				// Hi ha coincidencia, comprovo la cadena:
				for (j = i; j < cadena.length(); j++) {

					// Mentre hi hagi coincidencies, trobada serà true
					trobada = (cadena.charAt(j) == buscarCadena.charAt(posicio));
					
					// La cadena mai serà igual, ja que hi ha almenys un caràcter diferent
					if (!trobada) break;

					// Incremento posició, per buscar el següent caràcter
					if (trobada) posicio++;
					
					// S'ha trobat la paraula completa, no cal que segueixis buscant, surt del bucle
					if (posicio == buscarCadena.length()) break;
					
				}
				
				// S'ha trobat la cadena sencera
				if (trobada) break;
			}
				
			// Cas en que s'ha trobat la cadena sencera
			if (trobada) JOptionPane.showMessageDialog(null, "La cadena, s'ha trobat");
			
			// No s'ha trobat la cadena
			if (!trobada) JOptionPane.showMessageDialog(null, "La cadena, no s'ha trobat");					
			
		} catch (Exception e) {
			// Error: cadena a buscar, més llarga que la cadena.
			if (buscarCadena.length() >= cadena.length()) JOptionPane.showMessageDialog(null,e.getMessage());
			// Altres errors
			else JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
		}
		
	}

}
