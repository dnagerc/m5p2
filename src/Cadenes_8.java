
import javax.swing.JOptionPane;
public class Cadenes_8 {

	public static void main(String[] args) {
		int nombre=-1, nombreDecimal=0;
		char[] cadenaBinari;
		boolean esValid=false,noBinari=false;
		
		try {
			// Demana mentre no sigui vàlid
			while (!esValid) {				
				nombre = Integer.parseInt(JOptionPane.showInputDialog(null,"Introdueix un binari per passar a decimal:"));
				esValid = (nombre >= 0);
			}
			
			// Converteixo a array de caràcters per a treballar millor.
			cadenaBinari = (nombre+"").toCharArray();
			
			// Elevo a la posició on es troba
			int exponent = -1;
			
			for( int i = cadenaBinari.length-1; i >= 0; i--) {

				exponent++;
				
				// Si és 1, elevo i acumulo
				if (cadenaBinari[i] == '1') nombreDecimal+= (int) (Math.pow(2, exponent));
				// Si és 0, em salto l'iteració
				else if (cadenaBinari[i] == '0') continue;
				// Si no és ni 1 ni 0, llanço excepció
				else {
					noBinari = !noBinari;
					throw new Exception("El nombre que has entrat, no és binari.");
				}
			}

			// Segueix la execució del programa...
			JOptionPane.showMessageDialog(null, "El número en decimal, és: "+nombreDecimal);
			
		} catch (Exception e) {
			// No binari
			if (noBinari) JOptionPane.showMessageDialog(null, e.getMessage());
			// Qualsevol altre error
			else JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
		}
		
	}

}
