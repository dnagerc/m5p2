
import javax.swing.JOptionPane;
public class Cadenes_3 {

	public static void main(String[] args) {

		// Declaracions de variables
		String paraula;
		byte minuscules=0, majuscules=0, digits=0;
		boolean esValida=false;
		
		try {

			// Demano un caràcter, encara no filtro si és un nombre o símbol
			paraula = JOptionPane.showInputDialog(null,"Entra una paraula: ","Validador de "
					+ "contrasenyes",JOptionPane.QUESTION_MESSAGE);

			// Longitud mínima de 8
			while(paraula.length() < 8) {
				// Recordo els criteris a l'usuari
				JOptionPane.showMessageDialog(null, "El password ha de tenir un mínim de 8 caràcters.\n"
						+ "El password ha de tenir almenys una lletra minúscula i almenys una lletra majúscula.\n"
						+ "Com a mínim ha de tenir dos dígits.");
				
				// Si és menor a 8, segueixo demanant.
				paraula = JOptionPane.showInputDialog(null,"Entra una paraula: ","Validador de "
						+ "contrasenyes",JOptionPane.QUESTION_MESSAGE);
			}

			for (int i = 0; i < paraula.length(); i++) {
				// Incrementar digits
				if (Character.isDigit(paraula.charAt(i))) digits++; 
					
				// Incrementar minuscules
				if (Character.isLowerCase(paraula.charAt(i))) minuscules++; 

				// Incrementar majuscules
				if (Character.isUpperCase(paraula.charAt(i))) majuscules++; 
				
				// Es cumpleix que la password és vàlida, en cas dels següents criteris
				if (digits > 1 && minuscules > 0 && majuscules > 0) esValida=true; 
				
				// Si és vàlida, no segueixis comprovant
				if (esValida) break;
			}
			if (!esValida) {
				JOptionPane.showMessageDialog(null, "El password ha de tenir un mínim de 8 caràcters.\n"
						+ "El password ha de tenir almenys una lletra minúscula i almenys una lletra majúscula.\n"
						+ "Com a mínim ha de tenir dos dígits.");
			} 

			if (esValida) JOptionPane.showMessageDialog(null, "La cadena: "+paraula+" és un password vàlid.");
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
		}
		
	}

}
