
import javax.swing.JOptionPane;
public class Cadenes_2 {

	public static void main(String[] args) {

		// Declaracions de variables
		String caracter;
		char entrada;
		boolean esVocal=false, esLletra=false;		
		
		try {
			// Demano un caràcter, encara no filtro si és un nombre o símbol.
			while((caracter = JOptionPane.showInputDialog("Entra una lletra: ")).length() != 1);
			
			// Comprovo si és lletra
			esLletra = Character.isLetter(caracter.charAt(0));

			// No és una lletra
			if (!esLletra) throw new Exception("El caràcter no és una lletra.");

			// L'entrada en aquest moment, ja és vàlida
			entrada = caracter.charAt(0);
			
			// Comprova si és vocal i assigna true a esVocal en cas que ho sigui
			if (entrada == 'a' || entrada == 'e' || entrada == 'i' || entrada == 'o' || entrada == 'u') esVocal=true;

			// Si és vocal, mostra això:
			if (esVocal) JOptionPane.showMessageDialog(null, caracter+" és una vocal.");

			// Si no és vocal
			if (!esVocal) JOptionPane.showMessageDialog(null, caracter+" és una consonant.");									
			
		} catch (Exception e) {
			// Comprovar si no és lletra 
			if (!esLletra) JOptionPane.showMessageDialog(null, e.getMessage());
			// Qualsevol altre error
			else JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
			
		}
		
	}

}
