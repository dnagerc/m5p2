
import javax.swing.JOptionPane;
public class Cadenes_4 {

	public static void main(String[] args) {

		// Declaracions de variables
		String cadena, eliminarCadena;
		
		try {

			// Demano una cadena d'entrada
			cadena = JOptionPane.showInputDialog(null,"Entra una cadena: ","Cadena d'entrada:",JOptionPane.QUESTION_MESSAGE);

			// Demano el que és vol treure de la cadena
			eliminarCadena = JOptionPane.showInputDialog(null,"Entra una cadena a eliminar: ","Cadena a eliminar:",JOptionPane.WARNING_MESSAGE);
			
			// Mostrem la cadena amb caràcters eliminats a l'usuari
			JOptionPane.showMessageDialog(null, cadena.replaceAll(eliminarCadena, ""));
				
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
		}
		
	}

}
