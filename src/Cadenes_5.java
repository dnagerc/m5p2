
import javax.swing.JOptionPane;
public class Cadenes_5 {

	public static void main(String[] args) {

		// Declaracions de variables
		String cadena;
		int digits=0;

		try {

			// Demano una cadena d'entrada
			cadena = JOptionPane.showInputDialog(null,"Introdueix una cadena: ","Cadena "
					+ "d'entrada:",JOptionPane.QUESTION_MESSAGE);

			for (char caracter : cadena.toCharArray()) {
				// Si és dígit, acumulo digits
				if (Character.isDigit(caracter)) digits+=Integer.parseInt(caracter+"");
			}
			
			// Mostro el sumatori dels dígits
			JOptionPane.showMessageDialog(null, "La suma dels números apareguts és: "+digits);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Hi ha hagut un error a l'entrada, surts del programa.");
		}
		
	}

}
