
import javax.swing.JOptionPane;
public class Cadenes_1 {

	public static void main(String[] args) {

		// Declaracions de variables
		char[] cadenaCaracters;
		String caracterString;
		int repeticions=0, majuscules=0; 
		
		try {
			// Demano un caràcter (un caràcter, també pot ser un nombre, per si a la cadena hi ha un número de telèfon)
			while((caracterString = JOptionPane.showInputDialog("Entra un caràcter: ")).length() != 1);
			
			// Demano una cadena
			cadenaCaracters = JOptionPane.showInputDialog("Entra una cadena: ").toCharArray();			
			
			// Recorro la cadena de caràcters, acumulo les vegades que apareix el caràcter i compto les majúscules
			for (char lletra : cadenaCaracters) {

				// Si és el mateix caràcter, incremento el comptador
				if (lletra == caracterString.charAt(0)) repeticions++;
				
				// Si és majúscula, incremento el comptador
				if (Character.isUpperCase(lletra)) majuscules++;
			}
			
			// Mostro les sortides a l'usuari
			JOptionPane.showMessageDialog(null, "Aparicions del caràcter: "+caracterString+":"+repeticions);
			JOptionPane.showMessageDialog(null, "Nombre de majúscules: "+majuscules);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Hi ha hagut un error en l'entrada, surts del programa.");
		}
		
	}

}
